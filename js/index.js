document.addEventListener("DOMContentLoaded", () => {
    function mainWeatherBlock() {
        if (document.querySelector(".mainBlockWithWeather")) {
            checkJsonPackage();
        } else {
            console.log(false);
        }
    }

    async function checkJsonPackage(e) {
        const city = document.querySelector(`input[name="city"]`).value;
        const server = `https://api.openweathermap.org/data/2.5/weather?units=metric&q=${city}&appid=165684e6b0bd71958faa4c911da3901f`;
        const response = await fetch(server, {
            method: "GET",
        });
        const responseResult = await response.json();
        console.log(responseResult);

        if(response.ok){
            getWeather(responseResult);
        } else {
            console.log(false + "response: error;")
        }
    }

    function getWeather(data){
        const obj = {
            nameCity: data.name,
            tempCity: Math.round(data.main.temp) + '°С',
            iconWeatherCity: data.weather[0].icon,
            weatherStatus: data.weather[0].main,
            feelsLikes: Math.floor(data.main.feels_like),
        }

        const mainBlockWithWeather = document.getElementById('mainBlockWithWeather');

        if(document.querySelector('input[name="userAgent"]').value == 'Mobile'){
            mainBlockWithWeather.style.backgroundColor = `#8077ac`;
            mainBlockWithWeather.style.boxShadow = `.5rem .5rem 1rem #8077ac`;
        } else {
            mainBlockWithWeather.style.backgroundColor = `#77ac77`;
            mainBlockWithWeather.style.boxShadow = `.5rem .5rem 1rem #77ac77`;
        }

        document.querySelector(`.city`).innerHTML = obj.nameCity;
        document.querySelector(`.degrees`).innerHTML = obj.tempCity;
        document.querySelector(`.iconWeather img`).src = `https://api.openweathermap.org/img/w/${obj.iconWeatherCity}.png`;
        document.querySelector(`.status`).innerHTML = obj.weatherStatus;
        document.querySelector(`.feelsLikes`).innerHTML = `<span class="feelsSt">Feels like:</span> ` + obj.feelsLikes + `°С`;
    }


    
    mainWeatherBlock();
    setInterval(mainWeatherBlock, 5 * 60 * 1000);
});