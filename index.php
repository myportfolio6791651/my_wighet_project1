<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>WeatherWighet</title>
    <link rel="icon" href="./img/material-symbols_weather-mix-outline.ico" type="image/x-icon">
    <link rel="stylesheet" href="./css/reset.css?v=<?php echo uniqid(); ?>">
    <link rel="stylesheet" href="./css/style.css?v=<?php echo uniqid(); ?>">
    <script src="./js/index.js?v=<?php echo uniqid(); ?>"></script>
</head>

<body>
    <aside class="widgetBlockWeather">
        <div class="mainBlockWithWeather" id="mainBlockWithWeather">
            <div class="weather">
                <p class="city"></p>
                <p class="status"></p>
                <p class="degrees"></p>
                <p class="feelsLikes"></p>
            </div>
            <div class="blockDataWeather">
                <div class="iconWeather">
                    <img src="">
                </div>
                <div class="blockDataTime">
                    <div class="blockSwap">
                        <p>DATE: <?= date('d:m:o',strtotime('now'))?></p>
                    </div>
                </div>
            </div>
        </div>
    </aside>
    <div style="display: none;">
        <?php
        function getUserIP()
        {
            if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
                $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
                $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
            }
            $client = @$_SERVER['HTTP_CLIENT_IP'];
            $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
            $remote = $_SERVER['REMOTE_ADDR'];

            if (filter_var($client, FILTER_VALIDATE_IP)) {
                $ip = $client;
            } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
                $ip = $forward;
            } else {
                $ip = $remote;
            }

            return $ip;
        }

        $access_token = 'e92d6d1edcceec';
        $ip = getUserIP();

        if ($ip) {
            $url = "http://ipinfo.io/{$ip}?token={$access_token}";
            $response = file_get_contents($url);

            if ($response !== false) {
                $data = json_decode($response);

                if ($data !== null && isset($data->city)) {
                    $city = $data->city;
                    echo "City: $city";

                    echo '<input type="hidden" name="city" value="' . htmlspecialchars($city) . '">';
                } else {
                    echo "JSON decoding error";
                }
            } else {
                echo "Error while receiving data";
            }
        } else {
            echo "Failed to obtain IP address";
        }

        $user_agent = $_SERVER['HTTP_USER_AGENT'];

        $mobile_false = false;

        $mobile_keywords = array('Mobile', 'iPhone', 'Android');

        foreach($mobile_keywords as $keyword){
            if (strpos($user_agent, $keyword) !== false) {
                $is_mobile = true;
                break;
            }
        }

        if ($is_mobile) {
            $new_user_agent = 'Mobile';
        } else {
            $new_user_agent = 'PC';
        }
        ?>

        <input type="hidden" name="ip" value="<?= getUserIP() ?>">
        <input type="hidden" name="userAgent" value="<?= $new_user_agent ?>">
    </div>
</body>

</html>